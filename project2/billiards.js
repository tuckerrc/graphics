//------------------------------------------------------------
// Global variables
//------------------------------------------------------------
var gl;
var mvMatrixLoc;
var fcolorLoc

var balls = [];
var ball_radius = 5;
count = 0;
var mousedown = false;
var mouse_cord = [];
var hit; // sound var for ball hitting
var shot; // sound var for cue 

var Ball = function(){
    var d = new Date();
    this.cue = false;
    this.position = vec3(0.0,0.0,0.0);
    this.color = vec4(1.0,1.0,1.0,1.0);
    this.velocity = vec3(0.0,0.0,0.0);
    this.last_render = d.getTime();
}

function sound(src) {
    this.sound = document.createElement("audio");
    this.sound.src = src;
    this.sound.setAttribute("preload", "auto");
    this.sound.setAttribute("controls", "none");
    this.sound.style.display = "none";
    document.body.appendChild(this.sound);
    this.play = function(){
        this.sound.play();
    }
    this.stop = function(){
        this.sound.pause();
    }
}

function animate() {
    // TODO: compute elapsed time from last render and update the balls
    // positions and velocities accordingly.

    for(var i=0; i < balls.length; i++){
      var d = new Date();
      var n = d.getTime();
      var t = n - balls[i].last_render;
      var normal;
      var collide = false;
      // Acceleration
      update = friction(balls[i].position, balls[i].velocity, t/1000);
      balls[i].position = update.pos;
      balls[i].velocity = update.vel;
      balls[i].last_render = n;

      // Wall Reflection
      if(balls[i].position[0] >= 1-(ball_radius*.01)){ balls[i].velocity[0] = -balls[i].velocity[0]  }
      if(balls[i].position[1] >= 1-(ball_radius*.01)){ balls[i].velocity[1] = -balls[i].velocity[1]  }
      if(balls[i].position[0] <= -1+(ball_radius*.01)){ balls[i].velocity[0] = -balls[i].velocity[0]  }
      if(balls[i].position[1] <= -1+(ball_radius*.01)){ balls[i].velocity[1] = -balls[i].velocity[1]  }

      for(var j=i + 1; j < balls.length; j++){
        if(j!=i){
          var collide = isCollision(balls[i],balls[j])
        }

        if(collide){
          var normal = normalize(subtract(balls[i].position,balls[j].position));
          var normal_rev = negate( normal );

          var v1 = balls[i].velocity;
          var v2 = balls[j].velocity;

          var vn1 = scale(dot( v1, normal_rev ), normal_rev);
          var vn2 = scale(dot( v2, normal ), normal);

          var vt1 = subtract(vn1, v1);
          var vt2 = subtract(vn2, v2);

          balls[i].velocity = negate(add(vt1, vn2));
          balls[j].velocity = add(vt2, vn1);
          
          hit.play();
          if(i === 0 && j ===1){
            console.log(balls[i].velocity);
            console.log(balls[j].velocity);
          }
        }
      }

    }
}

function isCollision(ball1, ball2){
  var rad = ball_radius*0.01;
  var dx = (ball1.position[0] + rad) - (ball2.position[0] + rad);
  var dy = (ball1.position[1] + rad) - (ball2.position[1] + rad);
  var d = Math.sqrt(dx * dx + dy * dy);

  if (d < (rad + rad)){
    return true;
  }
  return false;
}

function friction(position, velocity, time){
  // position and velacity are vec3 arrays
  // time is in seconds (ms/1000)
  // v(t) = a * t + v(0)  -- equation {1}
  // x(t) = 0.5 * a * t^2 + v(0) * t + x(0) -- equation {2}

  // Initialize position and velocity vars
  var vx = velocity[0];
  var vy = velocity[1];
  var xx = position[0];
  var xy = position[1];
  var xxt = xx;
  var xyt = xy;
  var vxt = vx;
  var vyt = vy;

  // Initialize acceleration and theta vars
  var a = 0.5;
  var ax;
  var ay;
  var theta_1;

  // calculate acceleration
  if(vx === 0){ // no y accel
    ay = a;
  } else if ( vy === 0) { // no x accel
    ax = a;
  } else { // calculate x and y components of acceleration
    theta_1 = Math.atan(Math.abs(vx)/Math.abs(vy));
    ax = Math.sin(theta_1) * a;
    ay = Math.cos(theta_1) * a;
  }

  if(vx != 0){ // calculate now x velocity and position only if it is moving in that direction
      if (vx > 0){ ax = -ax; } // if velocity is positive make acceleration negative
      vxt = ax * time + vx; // equation 1
      xxt = (0.5 * ax * (time*time)) + vx * time + xx; // equation 2
      if(vxt > 0 && ax > 0){ // the ball stops when is acceleration and velocity are in the same direction
        vxt = 0;
      } else if (vxt < 0 && ax < 0){
        vxt = 0;
      }
  }

  if(vy != 0){ // calculate now y velocity and position only if it is moving in that direction
      if (vy > 0){ ay = -ay; } // if velocity is positive make acceleration negative
      vyt = ay * time + vy;
      xyt = (0.5 * ay * (time*time)) + vy * time + xy;
      if(vyt > 0 && ay > 0){ // the ball stops when is acceleration and velocity are in the same direction
        vyt = 0;
      } else if (vyt < 0 && ay < 0){
        vyt = 0;
      }
  }

  pos_end = vec3(xxt,xyt,0.0);
  vel_end = vec3(vxt,vyt,0.0);

  return {
    pos : pos_end,
    vel : vel_end
  }
}

function tick() {
    requestAnimFrame(tick);
    render();
    animate();
}

//------------------------------------------------------------
// render()
//------------------------------------------------------------
function render() {
    gl.clear(gl.COLOR_BUFFER_BIT);
    for(var i = 0; i < balls.length; i++){
      gl.uniform4fv(fcolorLoc, balls[i].color);

    	mvMatrix = translate(balls[i].position);
    	// mvMatrix = mult(mvMatrix, translate(balls[i].velocity));
    	gl.uniformMatrix4fv(mvMatrixLoc, false, flatten(mvMatrix));
    	gl.drawArrays(gl.TRIANGLE_FAN, 0, 360);
    	//console.log(balls[i].position[1][1]);
    }
}

//------------------------------------------------------------
// Initialization
//------------------------------------------------------------
/**
 * Initializes webgl, shaders and event callbacks.
 */
window.onload = function init() {
    var canvas = document.getElementById('gl-canvas');
    var canvasWidth = canvas.width;
    var canvasHeight = canvas.height;

    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn't available");
    }

    //----------------------------------------
    // Configure WebGL
    //----------------------------------------
    gl.viewport(0, 0, canvasWidth, canvasHeight);
    gl.clearColor(0.004, 0.525, 0.302, 1.0); //	0.4, 52.5, 30.2

    //----------------------------------------
    // TODO: load shaders and initialize attribute
    // buffers
    //----------------------------------------

    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);


    mvMatrixLoc = gl.getUniformLocation(program, "mvMatrix");
    fcolorLoc = gl.getUniformLocation(program, "fcolor");

    // Create circle buffer
    var circleBufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, circleBufferId);
    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    var circle = createCircleTrig(ball_radius, 360);

    scaleVertices(0.01, circle);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(circle), gl.STATIC_DRAW)

    gl.drawArrays(gl.LINE_LOOP, 0, circle.length);

    initiateBalls();
    //----------------------------------------
    // TODO: write event handlers
    //----------------------------------------
    document.getElementById( "resetButton" ).onclick = function () {
	     initiateBalls();
    };

    canvas.addEventListener("mousedown", function() {
      mousedown = true;
      var x = (event.clientX/canvas.width)*2.0-1.0;
      var y = (event.clientY/canvas.height)*2.0-1.0;
      mouse_cord.push(vec3(x,y,0.0));
    });

    canvas.addEventListener("mousemove", function() {
      if(mousedown){
        var x = (event.clientX/canvas.width)*2.0-1.0;
        var y = (event.clientY/canvas.height)*2.0-1.0;
        if(mouse_cord.length < 2){
          mouse_cord.push(vec3(x,y,0.0));
        } else {
          mouse_cord.pop();
          mouse_cord.push(vec3(x,y,0.0));
        }
      }
    });

    canvas.addEventListener("mouseup", mouseExit);

    canvas.addEventListener("mouseout", mouseExit);
    
    shot = new sound("sounds/shot.wav");
    hit = new sound("sounds/hit.wav");

    tick();
};

function mouseExit(canvas){
  if(mousedown){
    var canvas = document.getElementById('gl-canvas');
    var x = (event.clientX/canvas.width)*2.0-1.0;
    var y = (event.clientY/canvas.height)*2.0-1.0;
    var vy = -1*(mouse_cord[0][1] - mouse_cord[1][1]);
    var vx = (mouse_cord[0][0] - mouse_cord[1][0]);
    if(balls[0].velocity[0] === 0 && balls[0].velocity[1] === 0 ){
      balls[0].velocity = vec3(vx,vy,0.0);
      console.log(balls[0].velocity);
      shot.play();
    }
    //console.log(vx, vy);

  }
  mouse_cord.pop();
  mouse_cord.pop();
  //console.log("mouse up");
  mousedown = false;

}

function scaleVertices(s, vertices) {
    for (var i = 0; i < vertices.length; ++i) {
        vertices[i] = scale(s, vertices[i]);
    }
}

function createCircleTrig(radius = 1, num_breaks = 360) {
    // Returns an array with vertices to create a circle
    // @Params:
    // radius - enter the radius of the circle
    // breaks - number of vertices to create in the circle

    var step = (360 / num_breaks) * (Math.PI / 180);
    var vertices = []
    for (var i = 0; i < (2 * Math.PI); i = i + step) {
        var x = (Math.cos(i) * radius);
        var y = (Math.sin(i) * radius);
        vertices.push(vec2(x, y));
    }
    return vertices;
}

function initiateBalls() {
    balls = [];
    balls.push(new Ball());
    balls.push(new Ball());
    balls[0].cue = true;

    balls[0].position = vec3(0.0,-0.5,0.0);
    balls[1].position = vec3(0.,0.5,0.0);

    balls[1].color = vec4(1.0,1.0,0.0,1.0); // Yellow
}
