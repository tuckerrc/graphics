var objects = [
    // Red ball
    { type: 'sphere',
      pc: vec4(-0.5 ,-0.4,0.5,1.0),
      r: 0.25,
      color: vec3(0.25,0.0,0.0),
      color_specular: vec3(1.0,1.0,1.0)
    },
    // green ball
    { type: 'sphere',
      pc: vec4(0.5 ,0.450,0.30,1.0),
      r: 0.25,
      color: vec3(0.09,0.14,0.0),
      color_specular: vec3(1.0,1.0,1.0)
    },
    // blue ball
    { type: 'sphere',
      pc: vec4(0.8 ,0.30,1.30,1.0),
      r: 0.25,
      color: vec3(0.0,0.0,0.3),
      color_specular: vec3(1.0,1.0,1.0)
    },
    //purple ball
    { type: 'sphere',
      pc: vec4(0.5 ,-0.50,0.30,1.0),
      r: 0.25,
      color: vec3(0.1,0.0,0.14),
      color_specular: vec3(1.0,1.0,1.0)
    },
    // gray ball
    { type: 'sphere',
      pc: vec4(-0.7 ,0.50,3,1.0),
      r: 0.25,
      color: vec3(0.3,0.2,0.3),
      color_specular: vec3(1.0,1.0,1.0)
    },
    { type: 'sphere',
      pc: vec4(3.25,2.25,-2.0,1.0),
      r: 0.001,
      color: vec3(1.0,1.0,1.0),
      lightsource: true
    },
    { type: 'plane',
      q: vec4(0.0,-0.7,2.0,1.0),
      n: normalize(vec4(0.,0.50,-0.20,0.0)),
      color: vec3(0.4,0.4,0.4),
      color_specular: vec3(1.0,1.0,1.0)
    }
];

var lights = [];
for (var i = 0; i < objects.length; i++){
    if(objects[i].lightsource)
	      lights.push(objects[i]);
}

var debug = false;
//debug = true;

function getColorTestPattern(x, y) {
    
    y = (y+1)/2;
    return vec3(x, 1-x, y);
}

var count = 0;
var number = 0;

/**
 * x and y are the normalized device coordinates (-1, -1) to (1, 1).
 * Return a vec3 with the color at this (normalized) pixel coordinate.
 */
function getColor(x, y) {
    // TODO
    if(debug){ console.log("x", x, "y", y); }
    var p = vec4(x,y,0.0,1.0);
    var eye = vec4(0,0,-6.0,1.0);
    var d = vec4(0.0,0.0,1.0,0.0);
    d = subtract(p, eye);

    return trace(p,d,0);
}

function trace(p, d, step){
    if(debug){ console.log("p", p, "d", d, 'step', step); }
    var locCol, refCol, transCol;
    var allCol;
    var max_recursive_step = 1;
    var bckCol = vec3(0.0,0.0,0.0);
    if (step > max_recursive_step){
	      return bckCol;
    }

    var q = intersect(p,d);
    var q_distance;
    var q_atten;
    
    if (q === false){
	      return bckCol;
    } else {
	      q_distance = length(subtract(q.intersect, p));
	      q_atten = 1 / (.2 + 0.5 * q_distance + 0.5 * Math.pow(q_distance, 2));
	      
    }
    if (q.lightsource){
	      return mult(q_atten, q.color);
    }

    r = normalize(reflect(d, q.reflect_normal));
    
    locCol = phong(q, p, d);
    locCol = mult(q_atten, locCol);
    refCol = trace(q.intersect,r,step+1);
    allCol = add(locCol, scale(0.25, refCol));

    for(var i = 0; i < allCol.length; i++){
	      if(allCol[i] > 1){
	          allCol[i] = 1.0;
	      }
	      
    }

    if (debug){ console.log("color", allCol); }

    return allCol;
}


function intersect(p,d){
    var int_point = Number.MAX_VALUE;
    var ret_obj;
    for (var i = 0; i < objects.length; i++){
    	  var obj = objects[i];
    	  switch (obj.type){
    	  case "sphere":
    	      var p_pc = subtract(p, obj.pc);
    	      // Quadratic parameters
    	      var a = dot(d,d);
    	      var b = 2 * dot(p_pc, d);
    	      var c = dot(p_pc, p_pc) - Math.pow(obj.r,2);

    	      // square rooted value - if this is negative there is no intersect
    	      var x = Math.pow(b,2) - (4 * a * c)

    	      if( x >= 0 ){
    		        var ti = ( -b + Math.sqrt(x) ) / (2 * a);
    		        var tj = ( -b - Math.sqrt(x) ) / (2 * a);

		            // Use the smallest t value greater than zero
    		        if( ti > 0 && tj > 0 && Math.min(ti,tj) < int_point){
		                //console.log(ti, tj);
    		            int_point = Math.min(ti,tj);
    		            ret_obj = obj;
    		        } else if ( tj < 0 && ti > 0 && ti < int_point) {
    		            int_point = ti;
    		            ret_obj = obj;
    		        }
		            else if ( ti < 0 && tj > 0 && tj < int_point) {
    		            int_point = tj;
    		            ret_obj = obj;
    		        }
		            else if ( ti === tj && ti < int_point) {
    		            int_point = ti;
    		            ret_obj = obj;
    		        }
    	      }
    	      break;
    	  case "plane":
    	      var plane_q = obj.q;
    	      var plane_normal = obj.n;

    	      var d_dot_n = dot(d, plane_normal);
    	      var t_val = (dot(plane_q, plane_normal) - dot(p, plane_normal)) / d_dot_n;
    	      if( t_val <= 0){
    		        break;
    	      } else {
    		        if (t_val < int_point){
    		            int_point = t_val;
    		            ret_obj = obj;
    		        }

    	      }

    	      break;
    	  }

    }
    
    if (int_point === Number.MAX_VALUE){
	      return false;
    } else {
	      var t = int_point - 0.000000000001; // offset by a little so it does not intesect with itself

	      ret_obj.intersect = add(p, scale(t,d));
        ret_obj.distance = subtract(ret_obj.intersect, p);
	      if (ret_obj.type === "sphere"){
	          ret_obj.reflect_normal = subtract(ret_obj.intersect, ret_obj.pc);
	      } else if (ret_obj.type ==="plane"){
	          ret_obj.reflect_normal = ret_obj.n;
	      }

    }
    if( debug ){console.log('intersect', ret_obj.type, ret_obj.intersect, ret_obj.reflect_normal);}
    return ret_obj;
    
}


function reflect(d, n){
    var normalized_n = normalize(n); 
    var a = dot(d, normalized_n);
    
    var b = scale(a * 2, normalized_n);
    var dNew = subtract(d ,b);
    
    return dNew;
}

function phong(obj, point, direction){
    var light_ambient = vec3(0.4,0.4,0.4);
    var ambient = vec3(0.0,0.0,0.0);
    ambient = add(ambient, mult(light_ambient, obj.color));
    var diffuse = vec3(0.0,0.0,0.0);
    var specular = vec3(0.0,0.0,0.0);
    
    for(var i = 0; i < lights.length; i++){
	      var light_position = normalize(subtract(lights[i].pc, obj.intersect));
	      var light_distance = length(light_position);
	      var atten_factor = 1 / (0.2 + light_distance * 0.3 + Math.pow(light_distance,2)*0.2);
	      var diffuse_atten = scale(atten_factor,obj.color);
	      
	      var diffuse_int = mult(lights[i].color, diffuse_atten);
	      diffuse_int = mult(diffuse_int, Math.max(dot(obj.reflect_normal, light_position),0.0));
	      diffuse = add(diffuse, diffuse_int);
	      
	      var specular_v = normalize(subtract(obj.intersect, point));
	      var specular_r = reflect(light_position, normalize(obj.reflect_normal));
	      var specular_alpha = 75;
	      
	      var r_dot_v = Math.pow(Math.max(dot(specular_r, specular_v),0),specular_alpha);
	      //console.log(r_dot_v);
	      var specular_int = mult(obj.color_specular, r_dot_v);
	      specular = mult(lights[i].color, specular_int);
	      specular = scale(atten_factor, specular);
	      
    }
    
    diffuse = scale(1, diffuse);
    specular = scale(0.1, specular);
    //    return add(ambient,diffuse);
    var return_color = add(add(ambient, diffuse),specular);
    
    return return_color;

}
