List all of your extra features here.

1 - Webpage layout is done using the Bootstrap framework (requires jQuery) (jQuery is not used anywhere else)
    Note: This works best with an active internet connection
2 - 9 motion capture files are preloaded and can be chosen with the buttons above the canvas
3 - Camera motion can be toggled from center focus to dude focus
4 - Animation speed can be changed using A and D
5 - The Motion Capture FPS is calculated and displayed on screen